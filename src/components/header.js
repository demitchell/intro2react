export class Header extends React.Component {
    render() {
        return (
        <div>
            <img src="../dist/assets/marvel-logo.svg" />
            <div className="secondary-header">Character listing from Marvel movies</div>
        </div>
        );
    }
}