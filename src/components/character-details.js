import { SimpleListing } from './simple-listing.js';
export class CharacterDetails extends React.Component {
    render() {
        if(!this.props.characterName)
            return <div>(No selection has been made)</div>;
        return (
            <div>
                <h2>{this.props.characterName}</h2>
                <p>{this.props.characterData.description}</p>
                <article>
                    <section>
                        <SimpleListing title="Actor Listing" listClass="actor-listing" listing={this.props.characterData.actors}></SimpleListing>
                    </section>
                    <section>
                        <SimpleListing title="Movie Appearances" listClass="movie-appearances" listing={this.props.characterData.movies}></SimpleListing>
                    </section>
                </article>
            </div>
        );
    }
    constructor(props) {
        super(props);
    }
}
